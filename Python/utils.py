import unittest, doctest, math, random
import numpy as np
import math

##############################################################################
# Utils part
##############################################################################

def distance(pt1,pt2):
    """Computes the Euclidian distance between pt1 and pt2.

    >>> distance((1,1),(1,2))
    1.0
    
    """
    return math.sqrt((pt2[0]-pt1[0])*(pt2[0]-pt1[0])+(pt2[1]-pt1[1])*(pt2[1]-pt1[1]))

def euclidianMatrix(P,Q):
    """Computes the Euclidian matrix of the n times m distances.
    
    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> euclidianMatrix(P,Q)
    array([[ 1.        ,  1.41421356],
           [ 1.41421356,  1.        ]])
    """
    E = np.ones((len(P),len(Q)))
    E = np.multiply(E,-1)
    for i in range(0,len(P)):
        for j in range(0,len(Q)):
            E[i,j] = distance(P[i],Q[j])
    return E

def matrixInLaTeX(E,n,m,norm=100,hline="",bar=""):
    """Prints a  matrix in LaTeX code.
    
 P = [(1,1),(1,2)]
 Q = [(2,1),(2,2)]
 M = euclidianMatrix(P,Q)
 matrixInLaTeX(E)
\\begin{array}{|*{ 2 }{c|}} \hline
1.0\t&\t1.41\t\\\\ \hline
1.41\t&\t1.0\t\\\\ \hline
\\end{array}
"""
    print "$$\\begin{array}{",bar,"*{",m,"}{c",bar,"}}",hline
    for i in range(n):
        for j in range(m-1):
            if i==j:
                print "\\mathbf{",
            print round(norm*E[i,j])/norm,
            if i==j:
                print "}",
            print "\t&\t",
        if i==n-1:
            print "\\mathbf{",
        print round(norm*E[i,m-1])/norm,
        if i==n-1:
            print "}",
        print "\t\\\\",hline
    print "\\end{array}$$"

def randomCurve(n,left=0,bottom=0,right=100,top=100):
    """Generates a random curve of $n$ points 
    in a rectangle (left,bottom,right,top).

    >>> P = randomCurve(20)
    >>> Q = randomlyPerturbedCurve(P,1)

    """
    P = [] 
    for i in range(n):
        x = random.randint(left, right)
        y = random.randint(bottom, top)
        P.append((x,y))
    return P
class RandomCurveTest(unittest.TestCase):
    def test_basic(self):
        P = randomCurve(20)
        self.assertEqual(len(P),20)

def randomPointOnUnitCircle():
    """Generates a random point on the unit circle.
    """
    angleInRadians = random.random() * 2.0 * math.pi
    dx = math.cos(angleInRadians)
    dy = math.sin(angleInRadians)
    return (dx,dy)
        
def randomLongEdgedCurve(n,edgeLength=10):
    """Generates a random curve of $n$ points 
    such that each edge is of length $edgeLength$

    >>> P = randomCurve(20,10)
    >>> Q = randomlyPerturbedCurve(P,1)

    """
    x,y = randomPointOnUnitCircle()
    P = [(x,y)] 
    for i in range(n):
        dx,dy = randomPointOnUnitCircle()
        x += dx * edgeLength
        y += dy * edgeLength
        P.append((x,y))
    return P
class RandomLongEdgedCurveTest(unittest.TestCase):
    def test_basic(self):
        n = 100
        edgeLength = 10                
        P = randomLongEdgedCurve(n,10)
        for i in range(n-1):
            d = distance(P[i],P[i+1])
            # print i,'\t',P[i],'\t',P[i+1],'\t',int(d),'\t',edgeLength
            self.assertEqual(int(round(d)),edgeLength)
def randomlyPerturbedCurve(P,d=1):
    """Generates a perturbation @Q of a curve @P, so that the distance from @P to @Q is not as big as if generated separately.

    >>> P = [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6)]
    >>> Q = randomlyPerturbedCurve(P,1)
    """
    Q = []
    for x,y in P:
        xp = x+random.randint(-d, d)
        yp = y+random.randint(-d, d)
        Q.append((xp,yp))
    return Q
class RandomlyPerturbedCurveTest(unittest.TestCase):
    def test_basic(self):
        P = [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6)]
        self.assertEqual(len(P),6)
        Q = randomlyPerturbedCurve(P,1)
        self.assertEqual(len(Q),len(P))

def main():
    unittest.main()
    
if __name__ == '__main__':
    doctest.testmod()
    main()
