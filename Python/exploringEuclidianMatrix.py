import unittest, doctest, math, random
import numpy as np
import utils as utils
import naiveFrechet 

##############################################################################
# Main experiment
##############################################################################
                
def main():
    P = utils.randomCurve(5)
    Q = utils.randomCurve(5)
    # Q = utils.randomlyPerturbedCurve(P,2)
    print utils.euclidianMatrix(P,Q)
    unittest.main()
    
if __name__ == '__main__':
    doctest.testmod()
    main()

