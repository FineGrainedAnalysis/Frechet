import unittest, doctest, math, random
import numpy as np
import utils as utils

##############################################################################
# Classical computation of Discrete Frechet distance
##############################################################################

def recursive(dynamicProgrammingArray,i,j,P,Q):
    """
    Computes the Frechet distance between @P and @Q in @dynamicProgrammingArray by a simple dynamic programming recursion over @i and @j

    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> dynamicProgrammingArray = np.ones((len(P),len(Q)))
    >>> dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-1)
    >>> recursive(dynamicProgrammingArray,len(P)-1,len(Q)-1,P,Q)
    1.0

    """
    if dynamicProgrammingArray[i,j] > -1:
        return dynamicProgrammingArray[i,j]
    elif i == 0 and j == 0:
        dynamicProgrammingArray[i,j] = utils.distance(P[0],Q[0])
    elif i > 0 and j == 0:
        dynamicProgrammingArray[i,j] = max(
            recursive(dynamicProgrammingArray,i-1,0,P,Q),
            utils.distance(P[i],Q[0]))
    elif i == 0 and j > 0:
        dynamicProgrammingArray[i,j] = max(
            recursive(dynamicProgrammingArray,0,j-1,P,Q),
            utils.distance(P[0],Q[j]))
    elif i > 0 and j > 0:
        dynamicProgrammingArray[i,j] = max(
            min(
                recursive(dynamicProgrammingArray,i-1,j,P,Q),
                recursive(dynamicProgrammingArray,i-1,j-1,P,Q),
                recursive(dynamicProgrammingArray,i,j-1,P,Q)),
            utils.distance(P[i],Q[j]))
    else:
        dynamicProgrammingArray[i,j] = float("inf")
    return dynamicProgrammingArray[i,j]

def computation(P,Q):
    """ Computes the discrete frechet distance between two polygonal lines
    Algorithm: http://www.kr.tuwien.ac.at/staff/eiter/et-archive/cdtr9464.pdf
    P and Q are arrays of 2-element arrays (points)

    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> computation(P,Q)
    1.0

    """
    dynamicProgrammingArray = np.ones((len(P),len(Q)))
    dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-1)
    d = recursive(dynamicProgrammingArray,len(P)-1,len(Q)-1,P,Q)
    return d

class SimpleRecursiveComputationOfFrechetDistanceCurveTest(unittest.TestCase):
    def test_basic(self):
        P = [(1,1),(1,2)]
        Q = [(2,1),(2,2)]
        self.assertEqual(computation(P,Q),1.0)
    def test_random(self):
        for i in range(1,100):
            P = utils.randomCurve(20)
            Q = utils.randomCurve(20)
            d = computation(P,Q)
            assert(d<142)
    def test_randomlyPerturbatedCurves(self):
        for perturbation in range(1,10):
            for i in range(1,10):
                P = utils.randomCurve(20)
                Q = utils.randomlyPerturbedCurve(P,perturbation)
                d = computation(P,Q)
                assert(d <= 1.42*perturbation)
def main():
    unittest.main()
    
if __name__ == '__main__':
    doctest.testmod()
    main()

