import unittest, doctest, math, random
import numpy as np
import utils as utils
import naiveFrechet 


##############################################################################
# Diagonal first adaptive algorithm
##############################################################################

def approximation(P,Q,w,t):
    bReached = False
    n = len(P)
    m = len(Q)
    assert( m <= n )
    assert( m > 0 )
    def e(i,j):
        d = utils.distance(P[i],Q[j])
        if d < t:
            if (i-j) >= w or (j-i) >= w:
                bReached = True
            return d
        else:
            return float("inf")
    oldRow = np.ones(n)
    oldColumn = np.ones(m)
    oldRow[0] = oldColumn[0] = e(0,0)
    for s in range(1,m):
        newRow = np.ones(n)
        for i in range(max(1,s-w+1),s):
            newRow[i]  = max(e(i,s),min(
                oldRow[i],
                oldRow[i-1],
                newRow[i-1]))
        newColumn = np.ones(m)
        for j in range(max(1,s-w+1),s):
            newColumn[j]  = max(e(s,j),min(
                newColumn[j-1],
                oldColumn[j-1],
                oldColumn[j]))
        newColumn[s] = newRow[s] = max(e(s,s),min(
            newRow[s-1],
            newColumn[s-1],
            oldRow[s-1]))
        oldRow = newRow
        oldColumn = newColumn
    for s in range(m,n):
      newColumn = np.ones(m)
      for j in range(max(1,s-w+1),m):
          newColumn[j]  = max(e(s,j),min(
              oldColumn[j],
              oldColumn[j-1],
              newColumn[j-1]))
      oldColumn = newColumn
    return bReached,newRow[n-1]
class approximationTest(unittest.TestCase):
    def test_identicalCurves(self):
        P = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6)]
        Q = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6)]
        w = 5
        t = 4
        self.assertEqual(approximation(P,Q,w,t), (False,0))

def diagonalFirstComputationOfFrechetDistance(P,Q):
    """ Compute the discrete Frechet distance by focusing on the shortest path in the euclidian matrix first.

    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> diagonalFirstComputationOfFrechetDistance(P,Q)
    1.0

    """
    dynamicProgrammingArray = np.ones((len(P),len(Q)))
    dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-1)
    return diagonalFirstRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,len(P)-1,len(Q)-1,P,Q,firstApproximationOfDiscreteFrechetDistance(P,Q,dynamicProgrammingArray))

def firstApproximationOfDiscreteFrechetDistance(P,Q,dynamicProgrammingArray):
    """ Compute the maximum on the diagonal of the euclidian matrix. 
    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> dynamicProgrammingArray = np.ones((len(P),len(Q)))
    >>> firstApproximationOfDiscreteFrechetDistance(P,Q,dynamicProgrammingArray)
    1.0
    """
    currentMax = utils.distance(P[-1],Q[-1])
    i = len(P)-2
    j = len(Q)-2
    while i>0 and j>0:
        currentMax = max(currentMax,utils.distance(P[i],Q[j]))
        i = i-1
        j = j-1
    while i>0:
        currentMax = max(currentMax,utils.distance(P[i],Q[j]))
        i = i-1
    while j>0:
        currentMax = max(currentMax,utils.distance(P[i],Q[j]))
        j = j-1
    return currentMax
    
def diagonalFirstRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,i,j,P,Q,currentMax):
    """
    Computes the discrete Frechet distance between @P and @Q in @dynamicProgrammingArray knowing that a path of "length" @currentMax already found.

    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> dynamicProgrammingArray = np.ones((len(P),len(Q)))
    >>> dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-1)
    >>> diagonalFirstRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,len(P)-1,len(Q)-1,P,Q,float("inf"))
    1.0

    """
    if dynamicProgrammingArray[i,j] > -1:
        return dynamicProgrammingArray[i,j]
    elif i == 0 and j == 0:
        dynamicProgrammingArray[i,j] = utils.distance(P[0],Q[0])
    elif i > 0 and j == 0:
        dynamicProgrammingArray[i,0] = utils.distance(P[i],Q[0])
        for ip in range(0,i):                    
            dynamicProgrammingArray[i,0] = max(
                utils.distance(P[ip],Q[0]),
                dynamicProgrammingArray[i,0]
            )
    elif i == 0 and j > 0:
        dynamicProgrammingArray[0,j] = utils.distance(P[0],Q[j])
        for jp in range(0,j):                    
            dynamicProgrammingArray[0,j] = max(
                utils.distance(P[0],Q[jp]),
                dynamicProgrammingArray[0,j]
            )
    elif i > 0 and j > 0:        
        leftTop = diagonalFirstRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,i-1,j-1,P,Q,currentMax)
        left = diagonalFirstRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,i-1,j,P,Q,currentMax)
        top = diagonalFirstRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,i,j-1,P,Q,currentMax)
        minimum = min(left,top,leftTop)
        dynamicProgrammingArray[i,j] = utils.distance(P[i],Q[j])
        if (minimum > dynamicProgrammingArray[i,j]):
            dynamicProgrammingArray[i,j] = minimum
    else:
        dynamicProgrammingArray[i,j] = float("inf")
    return dynamicProgrammingArray[i,j]


class diagonalFirstComputationOfFrechetDistanceCurveTest(unittest.TestCase):
    def test_basic(self):
        P = [(1,1),(1,2)]
        Q = [(2,1),(2,2)]
        self.assertEqual(diagonalFirstComputationOfFrechetDistance(P,Q),1.0)
    def test_completelyRandomCurves(self):
        for i in range(1,100):
            P = utils.randomCurve(20)
            Q = utils.randomCurve(20)
            self.assertEqual(diagonalFirstComputationOfFrechetDistance(P,Q),naiveFrechet.computation(P,Q))
    def test_randomlyPerturbatedCurves(self):
        for perturbation in range(1,10):
            for i in range(1,10):
                P = utils.randomCurve(20)
                Q = utils.randomlyPerturbedCurve(P,perturbation)
                self.assertEqual(diagonalFirstComputationOfFrechetDistance(P,Q),naiveFrechet.computation(P,Q))



                
def main():
    unittest.main()
    
if __name__ == '__main__':
    doctest.testmod()
    main()
