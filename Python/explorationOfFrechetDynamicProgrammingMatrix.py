import unittest, doctest, math, random
import numpy as np
import utils as utils
import naiveFrechet 
import bandedThresholdRecursive

##############################################################################
# First Adaptive tentative
##############################################################################
                
def adaptiveRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,i,j,P,Q):
    """
    Computes the Frechet distance between @P and @Q in @dynamicProgrammingArray by an improved dynamic programming recursion over @i and @j which skips some recursive calls when the distance is monotonically incrementing for a fixed point.     

    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> dynamicProgrammingArray = np.ones((len(P),len(Q)))
    >>> dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-1)
    >>> adaptiveRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,len(P)-1,len(Q)-1,P,Q)
    1.0

    """
    if dynamicProgrammingArray[i,j] > -1:
        return dynamicProgrammingArray[i,j]
    elif i == 0 and j == 0:
        dynamicProgrammingArray[i,j] = utils.distance(P[0],Q[0])
    elif i > 0 and j == 0:
        dynamicProgrammingArray[i,0] = utils.distance(P[i],Q[0])
        for ip in range(0,i):                    
            dynamicProgrammingArray[i,0] = max(
                utils.distance(P[ip],Q[0]),
                dynamicProgrammingArray[i,0]
            )
    elif i == 0 and j > 0:
        dynamicProgrammingArray[0,j] = utils.distance(P[0],Q[j])
        for jp in range(0,j):                    
            dynamicProgrammingArray[0,j] = max(
                utils.distance(P[0],Q[jp]),
                dynamicProgrammingArray[0,j]
            )
    elif i > 0 and j > 0:        
        leftTop = adaptiveRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,i-1,j-1,P,Q)
        left = adaptiveRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,i-1,j,P,Q)
        top = adaptiveRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,i,j-1,P,Q)
        minimum = min(left,top,leftTop)
        dynamicProgrammingArray[i,j] = utils.distance(P[i],Q[j])
        if (minimum > dynamicProgrammingArray[i,j]):
            dynamicProgrammingArray[i,j] = minimum
    else:
        dynamicProgrammingArray[i,j] = float("inf")
    return dynamicProgrammingArray[i,j]

def adaptiveComputationOfFrechetDistance(P,Q):
    """ First adaptive variant to compute the Discrete Frechet Distance between P and Q.

    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> adaptiveComputationOfFrechetDistance(P,Q)
    1.0

    """
    dynamicProgrammingArray = np.ones((len(P),len(Q)))
    dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-1)
    return adaptiveRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,len(P)-1,len(Q)-1,P,Q)

def dynamicProgrammingMatrix(P,Q):
    """ Return the Dynamic Programming Matrix after the computation of the Frechet distance,
    for closer inspection, in the hope of detecting some potential for an adaptive variant.

    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> dynamicProgrammingMatrix(P,Q)    
    array([[ 1.        ,  1.41421356],
           [ 1.41421356,  1.        ]])

    """
    dynamicProgrammingArray = np.ones((len(P),len(Q)))
    dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-1)
    d = adaptiveRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,len(P)-1,len(Q)-1,P,Q)
    return dynamicProgrammingArray

def numberOfSpotsUsedInDynamicProgrammingArray(P,Q):
    """ Count the number of recursive calls "saved" by the adaptive version.

    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> numberOfSpotsUsedInDynamicProgrammingArray(P,Q)
    0

    """
    dynamicProgrammingArray = np.ones((len(P),len(Q)))
    dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-1)
    d = adaptiveRecursiveComputationOfFrechetDistance(dynamicProgrammingArray,len(P)-1,len(Q)-1,P,Q)
    numberOfValuesUnUnsed = 0
    for i in range(1,len(P)):
        for j in range(1,len(Q)):
            if dynamicProgrammingArray[i,j] == -1 :
                numberOfValuesUnUnsed += 1
    return numberOfValuesUnUnsed

class AdaptiveRecursiveComputationOfFrechetDistanceCurveTest(unittest.TestCase):
    def test_singlePoint(self):
        P = [(2,1)]
        Q = [(1,1)]
        self.assertEqual(adaptiveComputationOfFrechetDistance(P,Q),1.0)
    def test_basic(self):
        P = [(1,1),(1,2)]
        Q = [(2,1),(2,2)]
        self.assertEqual(adaptiveComputationOfFrechetDistance(P,Q),1.0)
    def test_completelyRandomCurves(self):
        for i in range(1,100):
            P = utils.randomCurve(20)
            Q = utils.randomCurve(20)
            self.assertEqual(adaptiveComputationOfFrechetDistance(P,Q),naiveFrechet.computation(P,Q))
    def test_randomlyPerturbatedCurves(self):
        for perturbation in range(1,10):
            for i in range(1,10):
                P = utils.randomCurve(20)
                Q = utils.randomlyPerturbedCurve(P,perturbation)
                self.assertEqual(adaptiveComputationOfFrechetDistance(P,Q),naiveFrechet.computation(P,Q))

                
def main():
    precision = 100

    P = utils.randomLongEdgedCurve(5,100)
    Q = utils.randomlyPerturbedCurve(P,10)
    E = utils.euclidianMatrix(P,Q)
    utils.matrixInLaTeX(E,len(P),len(Q),precision)
    F = dynamicProgrammingMatrix(P,Q)
    utils.matrixInLaTeX(F,len(P),len(Q),precision)
    touchBorder,A = bandedThresholdRecursive.approximationOfFrechetMatrix(P,Q,w=3,t=20)
    utils.matrixInLaTeX(A,len(P),len(Q),precision)

    P = utils.randomLongEdgedCurve(5,10)
    Q = utils.randomlyPerturbedCurve(P,10)
    # E = utils.euclidianMatrix(P,Q)
    # utils.matrixInLaTeX(E,len(P),len(Q),precision)
    # F = dynamicProgrammingMatrix(P,Q)
    # utils.matrixInLaTeX(F,len(P),len(Q),precision)
    touchBorder,A = bandedThresholdRecursive.approximationOfFrechetMatrix(P,Q,w=3,t=20)
    utils.matrixInLaTeX(A,len(P),len(Q),precision)

    P = utils.randomCurve(6,0,0,100,100)
    Q = utils.randomCurve(6,0,0,100,100)
    # E = utils.euclidianMatrix(P,Q)
    # utils.matrixInLaTeX(E,len(P),len(Q),precision)
    # F = dynamicProgrammingMatrix(P,Q)
    # utils.matrixInLaTeX(F,len(P),len(Q),precision)
    touchBorder,A = bandedThresholdRecursive.approximationOfFrechetMatrix(P,Q,w=3,t=80)
    utils.matrixInLaTeX(A,len(P),len(Q),precision)

    unittest.main()
    
if __name__ == '__main__':
    doctest.testmod()
    main()
