import unittest, doctest, math, random
import numpy as np
import utils as utils
import naiveFrechet 
import explorationOfFrechetDynamicProgrammingMatrix as ex


def approximationOfFrechetMatrix(P,Q,w=1,t=float("inf")):
    """Approximates the Frechet matrix between two sequence of points, by updating iteratively the lowest row and column of the dynamic program, limiting the computation to pairing (i,j) such that |i-j|<w and d(i,j)<t.
    """
    borderReached = False
    def e(i,j):
        d = utils.distance(P[i],Q[j])
        if d < t:
            if (i-j) >= w or (j-i) >= w:
                borderReached = True
            return d
        else:
            return float("inf")
    assert( len(Q) <= len(P) )
    assert( len(Q) > 0 )
    dynamicProgrammingArray = np.ones((len(P),len(Q)))
    dynamicProgrammingArray = np.multiply(dynamicProgrammingArray,-20)
    # Initialize "old" arrays
    dynamicProgrammingArray[0,0] = e(0,0)
    for i in range(1,min(w,len(P))):
        dynamicProgrammingArray[i,0] = max(e(i,0),dynamicProgrammingArray[i-1,0])
    for j in range(1,min(w,len(Q))):
        dynamicProgrammingArray[0,j] = max(e(0,j),dynamicProgrammingArray[0,j-1])
    # Compute values in the leftmost square of size w x w 
    for s in range(1,len(Q)):
        for i in range(max(1,s-w+1),s):
            dynamicProgrammingArray[i,s]  = max(e(i,s),min(
                dynamicProgrammingArray[i,s-1],
                dynamicProgrammingArray[i-1,s-1],
                dynamicProgrammingArray[i-1,s]
            ))
        for j in range(max(1,s-w+1),s):
            dynamicProgrammingArray[s,j]  = max(e(s,j),min(
                dynamicProgrammingArray[s-1,j],
                dynamicProgrammingArray[s-1,j-1],
                dynamicProgrammingArray[s,j-1]
            ))
        dynamicProgrammingArray[s,s]  = max(e(s,s),min(
            dynamicProgrammingArray[s-1,s],
            dynamicProgrammingArray[s-1,s-1],
            dynamicProgrammingArray[s,s-1]
        ))
    # Compute values in the rest of the array, propagating 
    for s in range(len(Q),len(P)):
      for j in range(max(1,s-w+1),len(Q)):
          dynamicProgrammingArray[s,j]  = max(e(s,j),min(
              dynamicProgrammingArray[s-1,j],
              dynamicProgrammingArray[s-1,j-1],
              dynamicProgrammingArray[s,j-1]
          ))
    return borderReached,dynamicProgrammingArray

# class approximationOfFrechetMatrixTest(unittest.TestCase):
#     def test_identicalCurves(self):
#         P = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6)]
#         Q = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6)]
#         w = 5
#         t = 4
#         self.assertEqual(
#             approximationOfFrechetMatrix(P,Q,w,t),
#             [[  0.,   1.,   2.,   3.,  float("inf"), -20.],
#              [  1.,   0.,   1.,   2.,   3.,  float("inf")],
#              [  2.,   1.,   0.,   1.,   2.,   3.],
#              [  3.,   2.,   1.,   0.,   1.,   2.],
#              [ float("inf"),   3.,   2.,   1.,   0.,   1.],
#              [-20.,  float("inf"),   3.,   2.,   1.,   0.]]
#         ).all()
        


def approximation(P,Q,w=1,t=float("inf")):
    """Approximates the Frechet distance between two sequence of points in space within O(n+m) and time within O(nm), by updating iteratively the lowest row and column of the dynamic program, limiting the computation to pairing (i,j) such that |i-j|<w and d(i,j)<t.

    # >>> P = [(1,1),(1,2)]
    # >>> Q = [(2,1),(2,2)]
    # >>> approximation(P,Q,2)
    # (False, 1.0)

    """
    assert( len(Q) <= len(P) )
    assert( len(Q) > 0 )
    borderReached = False
    old_horizontal = np.ones(len(P))
    old_horizontal = new_horizontal = np.multiply(old_horizontal,-1)
    old_vertical = np.ones(len(Q))
    old_vertical = new_vertical = np.multiply(old_vertical,-1)
    # Initialize "old" arrays
    d = utils.distance(P[0],Q[0])
    if d <= t:
        old_horizontal[0] = old_vertical[0] = d
    else:
        old_horizontal[0] = old_vertical[0] = float("inf")
    # Compute values in the leftmost square of size w x w 
    for s in range(1,min(w,len(Q))):
        for i in range(1,s):
            d = utils.distance(P[s],Q[i])
            if d <= t:
                new_horizontal[i] = max(d,
                                        min(
                                            new_horizontal[i-1],
                                            old_horizontal[i],
                                            old_vertical[s]
                                        )
                                    )
            else:
                new_horizontal[i] = float("inf")
        for j in range(1,s):
            d = utils.distance(P[j],Q[s])
            if d <= t:
                new_vertical[j] = max(d,min(
                    old_horizontal[s],
                    new_vertical[j-1],
                    old_vertical[j]
                ))
            else:
                new_vertical[j] = float("inf")
        old_horizontal = new_horizontal
        old_vertical = new_vertical
    # Compute values in the rest of the diagonal
    # if w < len(Q):
    #   for s in range(w,len(Q)):
    #     for i in range((s-w),(s+w)):
    #       new_horizontal[i] = max(utils.distance(P[s],Q[i]),min(old_horizontal[i-1],old_horizontal[i],old_vertical[j]))
    #     for j in range((s-w),(s+w)):
    #       new_vertical[j] = max(utils.distance(P[j],Q[s]),min(old_horizontal[i],old_vertical[i-1],old_vertical[j]))
    #     old_horizontal = new_horizontal
    #     old_vertical = new_vertical
    # # Compute values in the rest of the array, propagating horizontally
    # for s in range(w+len(Q),len(P)):
    #   for j in range(1,s):
    #     new_vertical[j] = max(utils.distance(P[j],Q[s]),min(old_horizontal[i],old_vertical[i-1],old_vertical[j]))
    #   old_vertical = new_vertical
    return borderReached,new_vertical[len(Q)-1]


# class approximationTest(unittest.TestCase):
#     def test_singlePoint(self):
#         P = [(1,1)]
#         Q = [(1,2)]
#         self.assertEqual(approximation(P,Q,w=100,t=100),(False, 1.0))
#     def test_identicalCurve(self):
#         P = [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6)]
#         Q = [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6)]
#         self.assertEqual(approximation(P,Q,w=100,t=100),(False, 0.0))
#     def test_parallelCurveWithNullThreshold(self):
#         P = [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6)]
#         Q = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6)]
#         self.assertEqual(approximation(P,Q,w=100,t=0),(False, float("inf")))
    # def test_parallelCurve(self):
    #     P = [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6)]
    #     Q = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6)]
    #     self.assertEqual(approximation(P,Q,1,10),(False, 1.0))
    # def test_basic(self):
    #     P = [(1,1),(1,2)]
    #     Q = [(2,1),(2,2)]
    #     self.assertEqual(approximation(P,Q,1,10),(False, 1.0))
    # def test_biggerMatrix(self):
    #     P = [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6)]
    #     Q = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6)]
    #     self.assertEqual(approximation(P,Q,1,10),(False, 1.0))

#     def test_random(self):
#         for i in range(1,100):
#             P = utils.randomCurve(20)
#             Q = utils.randomCurve(20)
#             d = approximation(P,Q)
#             assert(d<142)
#     def test_randomlyPerturbatedCurves(self):
#         for perturbation in range(1,10):
#             for i in range(1,10):
#                 P = utils.randomCurve(20)
#                 Q = utils.randomlyPerturbedCurve(P,perturbation)
#                 d = approximation(P,Q)
#                 assert(d <= 1.42*perturbation)
#     def test_completelyRandomCurves(self):
#         for i in range(1,100):
#             P = utils.randomCurve(20)
#             Q = utils.randomCurve(20)
#             self.assertEqual(approximation(P,Q),naiveFrechet.approximation(P,Q))
#     def test_randomlyPerturbatedCurves(self):
#         for perturbation in range(1,10):
#             for i in range(1,10):
#                 P = utils.randomCurve(20)
#                 Q = utils.randomlyPerturbedCurve(P,perturbation)
#                 self.assertEqual(approximation(P,Q),naiveFrechet.approximation(P,Q))




def computation(P,Q,w=float("inf"),t=float("inf")):
    """Computes the frechet distance between two sequence of points in space within O(n+m) and time within O(nm), 
by updating iteratively the lowest row and column of the dynamic program,
limiting the computation to pairing (i,j) such that |i-j|<w and d(i,j)<t

    >>> P = [(1,1),(1,2)]
    >>> Q = []
    >>> computation(P,Q)
    inf

    """
    if len(P)<len(Q): # Assume that len(P) >= len(Q)
      P,Q = Q,P
    if len(Q) == 0:
       return float("inf")
    w = 1
    boarderReached, t = approximation(P,Q,w)
    while borderReached and w < len(Q):
      w = 2*x
      borderReached,t = approximation(P,Q,w,t)
    return t


# class computationTest(unittest.TestCase):
#     def test_basic(self):
#         P = [(1,1),(1,2)]
#         Q = [(2,1),(2,2)]
#         self.assertEqual(computation(P,Q),1.0)
#     def test_random(self):
#         for i in range(1,100):
#             P = utils.randomCurve(20)
#             Q = utils.randomCurve(20)
#             d = computation(P,Q)
#             assert(d<142)
#     def test_randomlyPerturbatedCurves(self):
#         for perturbation in range(1,10):
#             for i in range(1,10):
#                 P = utils.randomCurve(20)
#                 Q = utils.randomlyPerturbedCurve(P,perturbation)
#                 d = computation(P,Q)
#                 assert(d <= 1.42*perturbation)
#     def test_completelyRandomCurves(self):
#         for i in range(1,100):
#             P = utils.randomCurve(20)
#             Q = utils.randomCurve(20)
#             self.assertEqual(computation(P,Q),naiveFrechet.computation(P,Q))
#     def test_randomlyPerturbatedCurves(self):
#         for perturbation in range(1,10):
#             for i in range(1,10):
#                 P = utils.randomCurve(20)
#                 Q = utils.randomlyPerturbedCurve(P,perturbation)
#                 self.assertEqual(computation(P,Q),naiveFrechet.computation(P,Q))
                
def main():
    P = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(2,9)]
    Q = [(2,1),(2,2),(2,3),(2,4),(2,6)]
    # Q = [(2,1),(2,2),(2,3),(2,4),(2,5),(2,6)]
    print "P = ", P
    print "Q = ", Q
    print "F(P,Q) = "
    print ex.dynamicProgrammingMatrix(P,Q)
    w = 6
    t = 4
    print "A(P,Q, w =",w,", t =",t,") = "
    borderReached,A = approximationOfFrechetMatrix(P,Q,w,t)
    print A
    if borderReached:
        print "Border Reached, get a better approximation."
    else:
        print "Border not reached, stop the computation."
    unittest.main()
    
if __name__ == '__main__':
    doctest.testmod()
    main()

