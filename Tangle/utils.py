import unittest, doctest, math, random
import numpy as np

def distance(pt1,pt2):
    """Computes the Euclidian distance between pt1 and pt2.

    >>> distance((1,1),(1,2))
    1.0
    
    """
    return math.sqrt((pt2[0]-pt1[0])*(pt2[0]-pt1[0])+(pt2[1]-pt1[1])*(pt2[1]-pt1[1]))

def euclidianMatrix(P,Q):
    """Computes the Euclidian matrix of the n times m distances.
    
    >>> P = [(1,1),(1,2)]
    >>> Q = [(2,1),(2,2)]
    >>> euclidianMatrix(P,Q)
    array([[ 1.        ,  1.41421356],
           [ 1.41421356,  1.        ]])
    """
    E = np.ones((len(P),len(Q)))
    E = np.multiply(E,-1)
    for i in range(0,len(P)):
        for j in range(0,len(Q)):
            E[i,j] = distance(P[i],Q[j])
    return E

def randomCurve(n,left=0,bottom=0,right=100,top=100):
    """Generates a random curve of $n points in the plane.    
    """
    P = [] 
    for i in range(n):
        x = random.randint(left, right)
        y = random.randint(bottom, top)
        P.append((x,y))
    return P
class RandomCurveTest(unittest.TestCase):
    def test_basic(self):
        P = randomCurve(20)
        self.assertEqual(len(P),20)

def randomlyPerturbedCurve(P,d=1):
    """Generates a perturbation @Q of a curve @P, so that the distance from @P to @Q is not as big as if generated separately.
    """
    Q = []
    for x,y in P:
        xp = x+random.randint(-d, d)
        yp = y+random.randint(-d, d)
        Q.append((xp,yp))
    return Q
class RandomlyPerturbedCurveTest(unittest.TestCase):
    def test_basic(self):
        P = [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6)]
        self.assertEqual(len(P),6)
        Q = randomlyPerturbedCurve(P,1)
        self.assertEqual(len(Q),len(P))

def main():
    unittest.main()
    
if __name__ == '__main__':
    doctest.testmod()
    main()
